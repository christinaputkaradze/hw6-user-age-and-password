// з колбеками
function createNewUser(askName, askLastName, askBirthday) {
    let newUser = {
        firstName: askName(),
        lastName: askLastName(),
        birthday: askBirthday(),
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge() {
            let age = new Date() - this.birthday;
            age = age / 1000 / 60 / 60 / 24 / 365;
            realAge = parseInt(age);
            return realAge;
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.getFullYear();
        },
    }

    Object.defineProperty(newUser, 'firstName', {
        value: this.firstName,
        writable: false,
        configurable: false,
    });

    Object.defineProperty(newUser, 'lastName', {
        value: this.lastName,
        writable: false,
        configurable: false,
    });
    
    //перевірка
    newUser.firstName = "xxx";
    newUser.lastName = "ooo";

    return newUser;
}

let result = createNewUser(askName, askLastName, askBirthday);

function askName() {
    do {
        firstName = prompt('Enter your name:');
    } while (!firstName);

    return firstName;
}

function askLastName() {
    do {
        lastName = prompt('Enter your lastname:');
    } while (!lastName);

    return lastName;
}

function askBirthday(){
    do {
        birthday = prompt('Enter your birthday in format dd.mm.yyyy:');

    } while (!birthday || birthday.length != 10);

    let arr = birthday.split('.');
    let day = arr[0];
    let month = arr[1];
    let year = arr[2];

    let birthdayDate = new Date(year, month, day);

    return birthdayDate;
}

console.log(result);
console.log(result.getLogin());
console.log(result.getAge());
console.log(result.getPassword());


